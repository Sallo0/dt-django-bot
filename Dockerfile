FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED=1

ENV PYTHONPATH=/usr/src/app/src

RUN apt update && apt install make

WORKDIR /usr/src/app

COPY Pipfile Pipfile.lock /usr/src/app/

RUN pip install pipenv && pipenv install --system --deploy && pipenv --clear

COPY . .
