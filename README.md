# Django bot for Doubletapp course

## Как это запустить??

### 1. Укажите необходимые переменные окружения в .env файлe

### 2. Установите необходимые зависимости
```
pipenv install 
pipenv shell
```
### 3. Миграции!
```
python src/manage.py migrate
```

### 4. Запускаем сервачок 
```
python src/manage.py runserver
```

### 5. И бычка выпускаем (запускаем бота)
```
python src/manage.py bot
```

### Все должно работать....
___

## Запуск в docker
### 1. Укажите необходимые переменные окружения в .env файлe

### 2. Создать volume с названием db-data
```
docker volume create db-data
```
### 3. Запустить 
```
docker-compose up -d
```