build_all:
	docker-compose build

pull_all:
	docker-compose pull

pull_app:
	docker pull ${IMAGE_APP}

push_all:
	docker-compose push



down:
	docker-compose down

up:
	docker-compose up -d




run_webhook:
	python src/manage.py webhook

run_polling:
	python src/manage.py polling



runserver:
	gunicorn config.wsgi:application -b 0.0.0.0:8000

migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

debug:
	python src/manage.py debug



test:
	docker-compose run server pytest ./src
	docker-compose down




lint:
	isort .
	black --config pyproject.toml .
	flake8 --config setup.cfg

check_lint:
	isort --check --diff .
	black --check --config pyproject.toml .
	flake8 --config setup.cfg




ci_check_lint:
	docker run --rm ${IMAGE_APP} make check_lint

ci_migrate:
	docker-compose run server python src/manage.py migrate
	docker-compose down

ci_collectstatic:
	docker-compose run server python src/manage.py collectstatic --no-input
	docker-compose down



nginx_start:
	sudo systemctl start nginx

nginx_stop:
	sudo systemctl stop nginx

nginx_reload:
	sudo nginx -s reload


####################################################
updev:
	docker-compose -f docker-compose-dev.yml up

devrunserver:
	gunicorn config.wsgi:application -b 0.0.0.0:8000 --reload

devbotrestart:
	docker-compose -f docker-compose-dev.yml restart bot

devshell:
	python src/manage.py shell_plus --print-sql