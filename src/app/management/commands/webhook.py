from django.core.management.base import BaseCommand

from app.internal.bot import TelegramBot
from config.settings import TG_TOKEN


class Command(BaseCommand):
    help = "This command starts telegram webhook bot"

    def handle(self, *args, **options):
        bot = TelegramBot(TG_TOKEN)
        bot.start_webhook()
