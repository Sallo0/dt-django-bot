from django.contrib import admin

from app.internal.admin import AdminUserAdmin, BankAccountAdmin, BankCardAdmin, TelegramUserAdmin, TransferAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
