from django.core.validators import MinLengthValidator
from django.db import models

from app.internal.models import BankCard
from app.internal.models.bank_account import BankAccount
from app.internal.models.telegram_user import TelegramUser


class Transfer(models.Model):
    source_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="transfers")
    source_card = models.ForeignKey(BankCard, on_delete=models.CASCADE, related_name="transfers")
    transfer_initiator = models.ForeignKey(TelegramUser, on_delete=models.CASCADE, related_name="transfers")
    target_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="receipts")
    target_card = models.ForeignKey(BankCard, on_delete=models.CASCADE, related_name="receipts")
    target_user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE, related_name="receipts")
    money_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
