from django.core.validators import MinLengthValidator
from django.db import models

from app.internal.models.bank_account import BankAccount
from app.internal.models.telegram_user import TelegramUser


class BankCard(models.Model):
    account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="cards")
    owner = models.ForeignKey(TelegramUser, on_delete=models.CASCADE, related_name="cards")
    card_number = models.CharField(max_length=16, unique=True, validators=[MinLengthValidator(16)])

    def __str__(self):
        return f"{self.card_number}"
