from django.db import models

from app.internal.models.telegram_user import TelegramUser


class AuthToken(models.Model):
    jti = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(TelegramUser, related_name="refresh_tokens", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)
