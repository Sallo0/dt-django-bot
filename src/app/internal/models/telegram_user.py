from django.db import models


class TelegramUser(models.Model):
    telegram_id = models.CharField(max_length=20, primary_key=True, verbose_name="Telegram ID")
    username = models.CharField(max_length=50, null=True, blank=True, verbose_name="Username")
    first_name = models.CharField(max_length=50, null=True, blank=True, verbose_name="First name")
    last_name = models.CharField(max_length=50, null=True, blank=True, verbose_name="Last name")
    phone_number = models.CharField(max_length=50, null=True, blank=True, unique=True, verbose_name="Phone number")
    password = models.CharField(max_length=128, null=True)
    favorites = models.ManyToManyField("self", symmetrical=False)

    def __str__(self):
        return f"{self.username}"
