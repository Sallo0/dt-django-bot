from django.core.validators import MinLengthValidator
from django.db import models

from app.internal.models.telegram_user import TelegramUser

RUB = "RUB"
EUR = "EUR"
USD = "USD"

CURRENCY_CHOICES = [("RUB", "Rubles"), ("EUR", "Euro"), ("USD", "Dollars")]


class BankAccount(models.Model):
    owner = models.ForeignKey(TelegramUser, on_delete=models.CASCADE, related_name="accounts")
    account_number = models.CharField(max_length=20, unique=True, validators=[MinLengthValidator(20)])
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    currency = models.CharField(max_length=100, choices=CURRENCY_CHOICES, default=RUB)

    def __str__(self):
        return f"{self.account_number}"
