from django.contrib import admin

from app.internal.models import AuthToken


@admin.register(AuthToken)
class AuthTokenAdmin(admin.ModelAdmin):
    list_display = ("jti", "user", "created_at", "revoked")
