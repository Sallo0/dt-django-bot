from django.contrib import admin

from app.internal.models import Transfer


@admin.register(Transfer)
class TransferAdmin(admin.ModelAdmin):
    list_display = (
        "source_account",
        "source_card",
        "transfer_initiator",
        "target_account",
        "target_card",
        "target_user",
        "money_amount",
    )
