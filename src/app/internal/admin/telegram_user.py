from django.contrib import admin

from app.internal.models import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = ("telegram_id", "username", "first_name", "last_name", "phone_number")
    search_fields = ("username", "first_name", "last_name", "phone_number")
