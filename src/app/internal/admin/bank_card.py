from django.contrib import admin

from app.internal.models import BankCard


@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    list_display = ("card_number", "owner", "account")
    search_fields = (
        "card_number",
        "owner__username",
        "owner__first_name",
        "owner__last_name",
        "account__account_number",
    )
    list_select_related = ("owner", "account")
