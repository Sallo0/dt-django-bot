from django.contrib import admin

from app.internal.models import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("account_number", "owner", "balance", "currency")
    search_fields = ("account_number", "owner__username", "owner__first_name", "owner__last_name")
    list_select_related = ("owner",)
    list_filter = ("currency",)
