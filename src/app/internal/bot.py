import os

from telegram.ext import Dispatcher, Updater

from app.internal.factories import RepoFactory, ServicesFactory
from app.internal.transport.bot import commands


class TelegramBot:
    def __init__(self, tg_token: str):
        self._updater = Updater(token=tg_token)
        self.repos_factory = RepoFactory()
        self.serv_factory = ServicesFactory(self.repos_factory)

    def _setup_updater(self):
        dispatcher: Dispatcher = self._updater.dispatcher

        for command in commands:
            dispatcher.add_handler(command(self.serv_factory).get_handler())

    def start_polling(self):
        self._setup_updater()
        self._updater.start_polling()
        self._updater.idle()

    def start_webhook(self):
        self._setup_updater()
        self._updater.start_webhook(
            listen="0.0.0.0",
            port=5000,
            url_path="bot",
            webhook_url=f'https://{os.getenv("DOMAIN")}/bot',
        )
