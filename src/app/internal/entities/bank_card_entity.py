from ninja import Schema

from app.internal.entities.bank_account_entity import BankAccountSchema
from app.internal.entities.telegram_user_entity import TelegramUserSchema


class BankCardSchema(Schema):
    owner: TelegramUserSchema
    account: BankAccountSchema
    card_number: str


class BankCardOut(BankCardSchema):
    owner: str
    account: str
    balance: float
    currency: str
