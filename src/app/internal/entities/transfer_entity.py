from ninja import Schema


class TransferSchema(Schema):
    source_account: str
    source_card: str
    transfer_initiator: str
    target_account: str
    target_card: str
    target_user: str
    money_amount: float
