from ninja import Schema


class AuthSchema(Schema):
    access_token: str
    refresh_token: str
