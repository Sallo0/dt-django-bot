from ninja import Schema


class TelegramUserSchema(Schema):
    telegram_id: str
    username: str = None
    first_name: str = None
    last_name: str = None
    phone_number: str = None


class TelegramUserOut(TelegramUserSchema):
    pass


class TelegramUserIn(Schema):
    username: str
