from ninja import Schema

from app.internal.entities.telegram_user_entity import TelegramUserSchema


class BankAccountSchema(Schema):
    owner: TelegramUserSchema
    account_number: str
    balance: float
    currency: str


class BankAccountOut(BankAccountSchema):
    owner: str
