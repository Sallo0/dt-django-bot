from ninja import Schema


class TransferIn(Schema):
    money_source: str
    card_number: str
    money_amount: float
