from .add_favorite_handler import AddFavoriteCommand
from .del_favorite_handler import DelFavoriteCommand
from .favorites_handler import FavoritesCommand
from .help_handler import HelpCommand
from .me_handler import MeCommand
from .my_account_handler import MyAccountCommand
from .my_bank_handler import MyBankCommand
from .my_card_handler import MyCardCommand
from .send_money_handler import SendMoneyCommand
from .set_fio_handler import SetFioCommand
from .set_password_handler import SetPasswordCommand
from .set_phone_handler import SetPhoneCommand
from .start_handler import StartCommand
from .transfers_handler import TransfersCommand

commands = [
    MyAccountCommand,
    MyBankCommand,
    MyCardCommand,
    SendMoneyCommand,
    StartCommand,
    MeCommand,
    SetPhoneCommand,
    SetFioCommand,
    FavoritesCommand,
    DelFavoriteCommand,
    AddFavoriteCommand,
    HelpCommand,
    TransfersCommand,
    SetPasswordCommand,
]
