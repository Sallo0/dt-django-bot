from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import GREETINGS_MESSAGE
from app.internal.transport.bot.validators import exception_interceptor


class StartCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @exception_interceptor
    def _start(self, update: Update, context: CallbackContext):
        if self.services.get_user_serv().get_user(update.effective_user.id) is None:
            self.services.get_user_serv().create_user(
                telegram_id=update.effective_user.id, username=update.effective_user.username
            )
        update.message.reply_text(GREETINGS_MESSAGE % update.effective_user.first_name)

    def get_handler(self):
        return CommandHandler("start", self._start)
