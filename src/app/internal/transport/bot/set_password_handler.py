from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import DATA_SAVED_MESSAGE
from app.internal.transport.bot.validators import exception_interceptor, user_exists

PASSWORD = 1


class SetPasswordCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @user_exists
    def _set_password(self, update: Update, context: CallbackContext):
        update.message.reply_text("Введи новый пароль")
        return PASSWORD

    @exception_interceptor
    def _password_handler(self, update: Update, context: CallbackContext):
        self.services.get_auth_serv().set_password_for_user(
            telegram_id=update.effective_user.id, password=update.message.text
        )
        update.message.reply_text(DATA_SAVED_MESSAGE)
        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("set_password", self._set_password),
            ],
            states={
                PASSWORD: [MessageHandler(Filters.all, self._password_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
