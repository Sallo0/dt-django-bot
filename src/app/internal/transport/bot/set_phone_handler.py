import re

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import DATA_SAVED_MESSAGE, ENTER_PHONE_MESSAGE, INCORRECT_PHONE_FORMAT_MESSAGE
from app.internal.transport.bot.validators import exception_interceptor, user_exists

PHONE = 1


class SetPhoneCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @user_exists
    def _set_phone(self, update: Update, context: CallbackContext):
        update.message.reply_text(ENTER_PHONE_MESSAGE)
        return PHONE

    @exception_interceptor
    def _phone_handler(self, update: Update, context: CallbackContext):
        if not re.match(r"^(\+)?(\d){11,15}$", update.message.text):
            update.message.reply_text(INCORRECT_PHONE_FORMAT_MESSAGE)
            return ConversationHandler.END

        self.services.get_user_serv().set_phone_on_user(
            telegram_id=update.effective_user.id,
            phone=update.message.text,
        )
        update.message.reply_text(DATA_SAVED_MESSAGE)
        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("set_phone", self._set_phone),
            ],
            states={
                PHONE: [MessageHandler(Filters.all, self._phone_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
