from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.exceptions import NotEnoughMoneyException, SameAccountException, UserDoesntExistException
from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import (
    ACCOUNT_DOES_NOT_EXISTS_MESSAGE,
    ANY_CARD_DOESNT_EXIST,
    CARD_DOES_NOT_EXISTS_MESSAGE,
    CHOOSE_RECIPIENT_CARD_MESSAGE,
    DATA_SAVED_MESSAGE,
    ENTER_ACCOUNT_NUMBER_MESSAGE,
    ENTER_CARD_NUMBER_MESSAGE,
    ENTER_RECIPIENT_USERNAME_MESSAGE,
    NOT_ENOUGH_MONEY_MESSAGE,
    SAME_ACCOUNT_ERROR_MESSAGE,
    USER_CARD_DOES_NOT_EXISTS_MESSAGE,
    USER_DOESNT_EXIST_MESSAGE,
    WRONG_TRANSFER_METHOD_MESSAGE,
    YOUR_CARDS_MESSAGE,
)
from app.internal.transport.bot.validators import (
    exception_interceptor,
    user_exists,
    user_have_bank_card,
    user_have_phone_number,
)

ENTER_METHOD = 1
ENTER_USERNAME = 2
ENTER_DESTINATION = 3
ENTER_MONEY_SOURCE = 4
ENTER_AMOUNT = 5


class SendMoneyCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory
        self._user_conv_storage = {}

    @user_exists
    @user_have_phone_number
    @user_have_bank_card
    def _send_money(self, update: Update, context: CallbackContext):
        update.message.reply_text("Какой способ?\n/username \n/card_number \n/account_number")
        return ENTER_METHOD

    @exception_interceptor
    def _method_entered_handler(self, update: Update, context: CallbackContext):
        method = update.message.text
        user_tg_id = update.effective_user.id
        self._user_conv_storage[update.effective_user.id] = {"method": update.message.text}

        if method == "/username":
            favorites = self.services.get_favorites_serv().get_favorites(user_tg_id)
            favorites_usernames_commands = [f"\n/{user.username}" for user in favorites]
            update.message.reply_text(ENTER_RECIPIENT_USERNAME_MESSAGE + "".join(favorites_usernames_commands))
            return ENTER_USERNAME

        elif method == "/card_number":
            update.message.reply_text(ENTER_CARD_NUMBER_MESSAGE)

        elif method == "/account_number":
            update.message.reply_text(ENTER_ACCOUNT_NUMBER_MESSAGE)

        else:
            update.message.reply_text(WRONG_TRANSFER_METHOD_MESSAGE)
            return ConversationHandler.END

        return ENTER_DESTINATION

    @exception_interceptor
    def _username_entered_handler(self, update: Update, context: CallbackContext):
        target_username = update.message.text.lstrip("/")

        try:
            target_user_cards = self.services.get_bank_serv().get_user_cards_by_username(target_username)
        except UserDoesntExistException:
            update.message.reply_text(USER_DOESNT_EXIST_MESSAGE)
            return ConversationHandler.END

        if not target_user_cards:
            update.message.reply_text(ANY_CARD_DOESNT_EXIST)
            return ConversationHandler.END

        reply_keyboard = [f"\n/{card.card_number}" for card in target_user_cards]
        update.message.reply_text(CHOOSE_RECIPIENT_CARD_MESSAGE + "".join(reply_keyboard))
        return ENTER_DESTINATION

    @exception_interceptor
    def _money_destination_entered_handler(self, update: Update, context: CallbackContext):
        user_tg_id = update.effective_user.id
        destination = update.message.text.lstrip("/")
        self._user_conv_storage[user_tg_id]["destination"] = destination

        if self._user_conv_storage[user_tg_id]["method"] == "/account_number":
            if self.services.get_bank_serv().get_account(destination) is None:
                update.message.reply_text(ACCOUNT_DOES_NOT_EXISTS_MESSAGE)
                return ConversationHandler.END

        elif self._user_conv_storage[user_tg_id]["method"] == "/card_number":
            if self.services.get_bank_serv().get_card(destination) is None:
                update.message.reply_text(CARD_DOES_NOT_EXISTS_MESSAGE)
                return ConversationHandler.END

        user_cards = self.services.get_bank_serv().get_user_cards(update.effective_user.id)

        reply = [YOUR_CARDS_MESSAGE]
        reply.extend([f"/{card.card_number} - {card.balance}{card.currency}" for card in user_cards])

        update.message.reply_text("\n".join(reply))

        return ENTER_MONEY_SOURCE

    @exception_interceptor
    def _money_source_entered_handler(self, update: Update, context: CallbackContext):
        source = update.message.text.lstrip("/")
        user_tg_id = update.effective_user.id
        self._user_conv_storage[update.effective_user.id]["source"] = source

        user_card_numbers = [card.card_number for card in self.services.get_bank_serv().get_user_cards(user_tg_id)]

        if source not in user_card_numbers:
            update.message.reply_text(USER_CARD_DOES_NOT_EXISTS_MESSAGE)
            return ConversationHandler.END

        update.message.reply_text("Сколько деняг?")
        return ENTER_AMOUNT

    @exception_interceptor
    def _amount_entered_handler(self, update: Update, context: CallbackContext):
        user_tg_id = update.effective_user.id
        money_amount = float(update.message.text)
        method = self._user_conv_storage[update.effective_user.id]["method"]
        destination = self._user_conv_storage[user_tg_id]["destination"]
        money_source = self._user_conv_storage[user_tg_id]["source"]

        try:
            if method == "/username" or method == "/card_number":
                self.services.get_money_trans_serv().send_money_by_card(
                    user_tg_id, money_source, destination, money_amount
                )
            elif method == "/account_number":
                self.services.get_money_trans_serv().send_money_by_account(
                    user_tg_id, money_source, destination, money_amount
                )
        except SameAccountException:
            update.message.reply_text(SAME_ACCOUNT_ERROR_MESSAGE)
            return ConversationHandler.END
        except NotEnoughMoneyException:
            update.message.reply_text(NOT_ENOUGH_MONEY_MESSAGE)
            return ConversationHandler.END

        update.message.reply_text(DATA_SAVED_MESSAGE)
        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("send_money", self._send_money),
            ],
            states={
                ENTER_METHOD: [MessageHandler(Filters.command, self._method_entered_handler)],
                ENTER_USERNAME: [MessageHandler(Filters.all, self._username_entered_handler)],
                ENTER_DESTINATION: [
                    MessageHandler(Filters.regex(r"^/?(\d{16}|\d{20})$"), self._money_destination_entered_handler)
                ],
                ENTER_MONEY_SOURCE: [MessageHandler(Filters.regex(r"^/?\d{16}$"), self._money_source_entered_handler)],
                ENTER_AMOUNT: [MessageHandler(Filters.regex(r"\d+(\.\d{1,2})?"), self._amount_entered_handler)],
            },
            fallbacks=[],
        )
