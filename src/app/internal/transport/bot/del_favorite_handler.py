from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.exceptions import UserNotInFavoriteException
from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import (
    DATA_SAVED_MESSAGE,
    ENTER_FAVORITE_USERNAME_MESSAGE,
    FAVORITES_LIST_IS_EMPTY_MESSAGE,
    USER_NOT_IN_FAVORITES_MESSAGE,
)
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number

USERNAME = 1


class DelFavoriteCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @exception_interceptor
    @user_exists
    @user_have_phone_number
    def _del_favorite(self, update: Update, context: CallbackContext):
        user_tg_id = update.effective_user.id
        favorites = self.services.get_favorites_serv().get_favorites(user_tg_id)

        if not favorites:
            update.message.reply_text(FAVORITES_LIST_IS_EMPTY_MESSAGE)
            return ConversationHandler.END

        favorites_usernames_commands = [
            f"\n/{user.username}" for user in self.services.get_favorites_serv().get_favorites(user_tg_id)
        ]
        update.message.reply_text(ENTER_FAVORITE_USERNAME_MESSAGE + "".join(favorites_usernames_commands))
        return USERNAME

    @exception_interceptor
    def _username_handler(self, update: Update, context: CallbackContext):
        username = update.message.text.lstrip("/")
        user_tg_id = update.effective_user.id

        try:
            self.services.get_favorites_serv().delete_favorite(user_tg_id, username)
        except UserNotInFavoriteException:
            update.message.reply_text(USER_NOT_IN_FAVORITES_MESSAGE)
            return ConversationHandler.END
        update.message.reply_text(DATA_SAVED_MESSAGE)
        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("del_favorite", self._del_favorite),
            ],
            states={
                USERNAME: [MessageHandler(Filters.command, self._username_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
