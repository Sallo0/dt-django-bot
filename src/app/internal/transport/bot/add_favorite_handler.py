from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.exceptions import UserDoesntExistException
from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import (
    DATA_SAVED_MESSAGE,
    ENTER_FAVORITE_USERNAME_MESSAGE,
    USER_DOESNT_EXIST_MESSAGE,
)
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number

USERNAME = 1


class AddFavoriteCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @user_exists
    @user_have_phone_number
    def _add_favorite(self, update: Update, context: CallbackContext):
        update.message.reply_text(ENTER_FAVORITE_USERNAME_MESSAGE)
        return USERNAME

    @exception_interceptor
    def _username_handler(self, update: Update, context: CallbackContext):
        user_tg_id = update.effective_user.id
        username = update.message.text.lstrip("/")
        try:
            self.services.get_favorites_serv().add_favorite(user_tg_id, username)
        except UserDoesntExistException:
            update.message.reply_text(USER_DOESNT_EXIST_MESSAGE)
            return ConversationHandler.END
        update.message.reply_text(DATA_SAVED_MESSAGE)
        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("add_favorite", self._add_favorite),
            ],
            states={
                USERNAME: [MessageHandler(Filters.all, self._username_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
