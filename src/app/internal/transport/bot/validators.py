import functools
import logging

from telegram import Update
from telegram.ext import ConversationHandler

from app.internal.transport.bot.messages import (
    ANY_CARD_DOESNT_EXIST,
    INTERNAL_ERROR_MESSAGE,
    NOT_REGISTERED_MESSAGE,
    SET_PHONE_MESSAGE,
)


def user_exists(func):
    @functools.wraps(func)
    def wrapper(obj, update: Update, *args, **kwargs):
        tg_id = update.effective_user.id
        if obj.services.get_user_serv().get_user(tg_id) is None:
            update.message.reply_text(NOT_REGISTERED_MESSAGE)
            return ConversationHandler.END
        return func(obj, update, *args, **kwargs)

    return wrapper


def user_have_bank_card(func):
    @functools.wraps(func)
    def wrapper(obj, update: Update, *args, **kwargs):
        tg_id = update.effective_user.id
        if not obj.services.get_bank_serv().get_user_cards(tg_id):
            update.message.reply_text(ANY_CARD_DOESNT_EXIST)
            return ConversationHandler.END
        return func(obj, update, *args, **kwargs)

    return wrapper


def user_have_phone_number(func):
    @functools.wraps(func)
    def wrapper(obj, update: Update, *args, **kwargs):
        tg_id = update.effective_user.id
        user = obj.services.get_user_serv().get_user(tg_id)
        if user.phone_number is None:
            update.message.reply_text(SET_PHONE_MESSAGE)
            return ConversationHandler.END
        return func(obj, update, *args, **kwargs)

    return wrapper


def exception_interceptor(func):
    @functools.wraps(func)
    def wrapper(obj, update: Update, *args, **kwargs):
        try:
            f_return = func(obj, update, *args, **kwargs)
        except Exception as e:
            logging.exception(e)
            update.message.reply_text(INTERNAL_ERROR_MESSAGE)
            return ConversationHandler.END
        return f_return

    return wrapper
