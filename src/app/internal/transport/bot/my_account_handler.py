import re

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.exceptions import AccountDoesntExistException
from app.internal.factories import ServicesFactory
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number

from .messages import (
    ACCOUNT_INFO_MESSAGE,
    ENTER_ACCOUNT_NUMBER_MESSAGE,
    INCORRECT_ACCOUNT_FORMAT_MESSAGE,
    USER_ACCOUNT_DOES_NOT_EXISTS_MESSAGE,
)

ACCOUNT_ENTERED = 1


class MyAccountCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @user_exists
    @user_have_phone_number
    def _my_card(self, update: Update, context: CallbackContext):
        update.message.reply_text(ENTER_ACCOUNT_NUMBER_MESSAGE)
        return ACCOUNT_ENTERED

    @exception_interceptor
    def _account_entered_handler(self, update: Update, context: CallbackContext):
        if not re.match(r"^(\d){20}$", update.message.text):
            update.message.reply_text(INCORRECT_ACCOUNT_FORMAT_MESSAGE)
            return ConversationHandler.END

        try:
            account = self.services.get_bank_serv().get_user_account_information(
                update.effective_user.id, update.message.text
            )
        except AccountDoesntExistException:
            update.message.reply_text(USER_ACCOUNT_DOES_NOT_EXISTS_MESSAGE)
            return ConversationHandler.END

        update.message.reply_text(ACCOUNT_INFO_MESSAGE % (account.account_number, account.balance, account.currency))

        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("my_account", self._my_card),
            ],
            states={
                ACCOUNT_ENTERED: [MessageHandler(Filters.all, self._account_entered_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
