from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number


class MeCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @exception_interceptor
    @user_exists
    @user_have_phone_number
    def _me(self, update: Update, context: CallbackContext):
        tg_id = update.effective_user.id

        answer = ""
        user = self.services.get_user_serv().get_user(tg_id)

        for key, value in user.dict().items():
            answer += f"{key}: {value}\n"
        update.message.reply_text(answer)

    def get_handler(self):
        return CommandHandler("me", self._me)
