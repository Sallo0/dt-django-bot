from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import DATA_SAVED_MESSAGE, ENTER_FIRST_NAME_MESSAGE, ENTER_LAST_NAME_MESSAGE
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number

FIRST_NAME = 1
LAST_NAME = 2


class SetFioCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @user_exists
    @user_have_phone_number
    def _set_fio(self, update: Update, context: CallbackContext):
        update.message.reply_text(ENTER_FIRST_NAME_MESSAGE)
        return FIRST_NAME

    @exception_interceptor
    def _first_name_handler(self, update: Update, context: CallbackContext):
        self.services.get_user_serv().set_first_name_on_user(update.effective_user.id, update.message.text)
        update.message.reply_text(ENTER_LAST_NAME_MESSAGE)
        return LAST_NAME

    @exception_interceptor
    def _last_name_handler(self, update: Update, context: CallbackContext):
        self.services.get_user_serv().set_last_name_on_user(update.effective_user.id, update.message.text)
        update.message.reply_text(DATA_SAVED_MESSAGE)
        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("set_fio", self._set_fio),
            ],
            states={
                FIRST_NAME: [MessageHandler(Filters.all, self._first_name_handler, pass_user_data=True)],
                LAST_NAME: [MessageHandler(Filters.all, self._last_name_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
