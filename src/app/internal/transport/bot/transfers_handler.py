from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number


class TransfersCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @exception_interceptor
    @user_exists
    @user_have_phone_number
    def _transfers(self, update: Update, context: CallbackContext):
        tg_id = update.effective_user.id

        transfers = self.services.get_money_trans_serv().get_user_transfers(tg_id)
        receipts = self.services.get_money_trans_serv().get_user_receipts(tg_id)
        result = ["```", "Твои отправления"]

        for transfer in transfers:
            result.append(f"{transfer.target_user} - {transfer.money_amount}")

        result.append("Твои получения")

        for receipt in receipts:
            result.append(f"{receipt.transfer_initiator} - {receipt.money_amount}")
        result.append("```")
        update.message.reply_text("\n".join(result), parse_mode="Markdown")

    def get_handler(self):
        return CommandHandler("transfers", self._transfers)
