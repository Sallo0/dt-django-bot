from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import (
    ACCOUNT_INFO_MESSAGE,
    CARD_INFO_MESSAGE,
    YOUR_ACCOUNTS_MESSAGE,
    YOUR_CARDS_MESSAGE,
)
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number


class MyBankCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @exception_interceptor
    @user_exists
    @user_have_phone_number
    def _my_bank(self, update: Update, context: CallbackContext):
        tg_id = update.effective_user.id

        accounts = self.services.get_bank_serv().get_user_accounts(tg_id)
        cards = self.services.get_bank_serv().get_user_cards(tg_id)
        result = ["```", YOUR_ACCOUNTS_MESSAGE]

        for account in accounts:
            result.append(ACCOUNT_INFO_MESSAGE % (account.account_number, account.balance, account.currency))

        result.append(YOUR_CARDS_MESSAGE)

        for card in cards:
            result.append(CARD_INFO_MESSAGE % (card.card_number, card.balance, card.currency, card.account))
        result.append("```")
        update.message.reply_text("\n".join(result), parse_mode="Markdown")

    def get_handler(self):
        return CommandHandler("my_bank", self._my_bank)
