GREETINGS_MESSAGE = "Здравсвтуй, %s! \n Для получения информации о возможностях бота напиши /help"
SET_PHONE_MESSAGE = "Чтобы продолжить укажи номер телефона через /set_phone"
NOT_REGISTERED_MESSAGE = "Чтобы продолжить зарегистрируйся через /start"
ENTER_PHONE_MESSAGE = "Введи номер телефона"
INCORRECT_PHONE_FORMAT_MESSAGE = "Неправильный формат мобильного номера"
DATA_SAVED_MESSAGE = "Сохранено!"

ENTER_FIRST_NAME_MESSAGE = "Введи имя"
ENTER_LAST_NAME_MESSAGE = "Введи фамилию"

ENTER_CARD_NUMBER_MESSAGE = "Введи номер карты"
INCORRECT_CARD_FORMAT_MESSAGE = "Неверный формат карты"
USER_CARD_DOES_NOT_EXISTS_MESSAGE = "У тебя нет такой карты 🤔"
YOUR_CARDS_MESSAGE = "Твои банковские карты:"
CARD_INFO_MESSAGE = "Карта %s с балансом %s %s. Счет: %s"

INTERNAL_ERROR_MESSAGE = "Внутренняя ошибка сервера"

ENTER_ACCOUNT_NUMBER_MESSAGE = "Введи номер счета"
INCORRECT_ACCOUNT_FORMAT_MESSAGE = "Неверный формат счета"
USER_ACCOUNT_DOES_NOT_EXISTS_MESSAGE = "У тебя нет такого счета 🤔"
YOUR_ACCOUNTS_MESSAGE = "Твои банковские счета:"
ACCOUNT_INFO_MESSAGE = "Счет %s с балансом %s %s"

NOT_ENOUGH_MONEY_MESSAGE = "Недостаточно средств"
SAME_ACCOUNT_ERROR_MESSAGE = "Нельзя одновременно списывать и зачислять деньги на один аккаунт"

ANY_CARD_DOESNT_EXIST = "Нет ни одной карты!"

ENTER_FAVORITE_USERNAME_MESSAGE = "Введи username избранного пользователя"
USER_NOT_IN_FAVORITES_MESSAGE = "Такого пользователя нет в списке"
FAVORITES_LIST_IS_EMPTY_MESSAGE = "Твой список избранных пользователей пуст!"
USER_DOESNT_EXIST_MESSAGE = "Такого пользователя не существует"
CARD_DOES_NOT_EXISTS_MESSAGE = "Такой карты не существует"
ACCOUNT_DOES_NOT_EXISTS_MESSAGE = "Такого счета не существует"

CHOOSE_RECIPIENT_CARD_MESSAGE = "Выбери карту получателя"
ENTER_RECIPIENT_USERNAME_MESSAGE = "Введи имя получателя"

WRONG_TRANSFER_METHOD_MESSAGE = "Неверный способ перевода"

HELP_MESSAGE = """Что умеет этот бот???
    /help - выводит это сообщение
    /start - Зарегистрироваться в боте
    /set_phone - Указать номер телефона
    /me - Вывести информацию о себе
    /set_fio - Указать имя и фамилию
    /my_bank - Информация о счетах и картах
    /my_card - Информация о конкретной карте
    /my_account - Информация о конкретном счете
    /send_money - Перевести деньги
    /favorites - Список избранных пользователей
    /del_favorite - Удалить пользователя из избранного
    /add_favorite - Добавить пользователя в избранное
    /transfers - Информация о получении и списании деняк
    /set_password - Установить пароль"""
