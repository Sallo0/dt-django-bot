from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import HELP_MESSAGE


class HelpCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    def _help(self, update: Update, context: CallbackContext):
        update.message.reply_text(HELP_MESSAGE)

    def get_handler(self):
        return CommandHandler("help", self._help)
