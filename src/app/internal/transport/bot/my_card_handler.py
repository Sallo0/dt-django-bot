import re

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.exceptions import CardDoesntExistException
from app.internal.factories import ServicesFactory
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number

from .messages import (
    CARD_INFO_MESSAGE,
    ENTER_CARD_NUMBER_MESSAGE,
    INCORRECT_CARD_FORMAT_MESSAGE,
    USER_CARD_DOES_NOT_EXISTS_MESSAGE,
)

CARD_ENTERED = 1


class MyCardCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @user_exists
    @user_have_phone_number
    def _my_card(self, update: Update, context: CallbackContext):
        update.message.reply_text(ENTER_CARD_NUMBER_MESSAGE)
        return CARD_ENTERED

    @exception_interceptor
    def _card_entered_handler(self, update: Update, context: CallbackContext):
        if not re.match(r"^(\d){16}$", update.message.text):
            update.message.reply_text(INCORRECT_CARD_FORMAT_MESSAGE)
            return ConversationHandler.END

        try:
            card = self.services.get_bank_serv().get_user_card_information(
                update.effective_user.id, update.message.text
            )
        except CardDoesntExistException:
            update.message.reply_text(USER_CARD_DOES_NOT_EXISTS_MESSAGE)
            return ConversationHandler.END

        update.message.reply_text(CARD_INFO_MESSAGE % (card.card_number, card.balance, card.currency, card.account))

        return ConversationHandler.END

    def get_handler(self):
        return ConversationHandler(
            entry_points=[
                CommandHandler("my_card", self._my_card),
            ],
            states={
                CARD_ENTERED: [MessageHandler(Filters.all, self._card_entered_handler, pass_user_data=True)],
            },
            fallbacks=[],
        )
