from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler

from app.internal.factories import ServicesFactory
from app.internal.transport.bot.messages import FAVORITES_LIST_IS_EMPTY_MESSAGE
from app.internal.transport.bot.validators import exception_interceptor, user_exists, user_have_phone_number


class FavoritesCommand:
    def __init__(self, serv_factory: ServicesFactory):
        self.services = serv_factory

    @exception_interceptor
    @user_exists
    @user_have_phone_number
    def _favorites(self, update: Update, context: CallbackContext):
        favorites = self.services.get_favorites_serv().get_favorites(update.effective_user.id)

        if not favorites:
            update.message.reply_text(FAVORITES_LIST_IS_EMPTY_MESSAGE)
            return ConversationHandler.END

        answer = "Твой список пользователей:\n"
        answer += "\n".join([user.username for user in favorites])
        update.message.reply_text(answer)

    def get_handler(self):
        return CommandHandler("favorites", self._favorites)
