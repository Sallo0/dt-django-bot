from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.factories import RepoFactory, ServicesFactory

services = ServicesFactory(RepoFactory())


class HTTPJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        if services.get_auth_serv().has_token_expired(token):
            return None

        telegram_id = services.get_auth_serv().get_telegram_id(token)
        if not telegram_id:
            return None

        user_model = services.get_user_serv().get_user(telegram_id)
        request.user = user_model

        return token
