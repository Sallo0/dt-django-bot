from http import HTTPStatus

from django.http import HttpRequest, HttpResponseBadRequest, HttpResponseNotFound, JsonResponse, HttpResponse
from ninja import NinjaAPI, Schema

from app.internal.entities import AuthSchema, TelegramUserOut, TelegramUserIn, BankCardOut
from app.internal.entities.money_transfer import TransferIn
from app.internal.factories import RepoFactory, ServicesFactory
from app.internal.transport.rest.auth import HTTPJWTAuth

api = NinjaAPI()

services = ServicesFactory(RepoFactory())


class UserSchema(Schema):
    phone: str
    password: str


class RefreshSchema(Schema):
    refresh_token: str


@api.get("/me", auth=HTTPJWTAuth())
def me(request: HttpRequest) -> TelegramUserOut | HttpResponseNotFound:
    user = services.get_user_serv().get_user(request.user.telegram_id)
    if user is None or not user.phone_number:
        return HttpResponseNotFound("no user or user's phone")

    return services.get_user_serv().get_user(request.user.telegram_id)


@api.post("/login")
def login(request: HttpRequest, user: UserSchema) -> JsonResponse | AuthSchema:
    db_user = services.get_user_serv().get_user_by_phone(user.phone)
    if user is None or not services.get_auth_serv().check_password(db_user, password=user.password):
        return JsonResponse({"detail": "Unauthorized"}, status=HTTPStatus.UNAUTHORIZED)

    return services.get_auth_serv().generate_access_and_refresh_tokens(db_user)


@api.post("/refresh")
def refresh(request: HttpRequest, token: RefreshSchema) -> HttpResponseBadRequest | AuthSchema:
    auth_token = services.get_auth_serv().get_token(token.refresh_token)
    if auth_token is None:
        return HttpResponseBadRequest("Invalid token")

    if auth_token.revoked:
        services.get_auth_serv().revoke_all_tokens(auth_token.user)
        return HttpResponseBadRequest("Invalid token")

    services.get_auth_serv().revoke_token(auth_token)
    if services.get_auth_serv().has_token_expired(token.refresh_token):
        return HttpResponseBadRequest("Invalid token")

    return services.get_auth_serv().generate_access_and_refresh_tokens(auth_token.user)


@api.put("/phone", auth=HTTPJWTAuth())
def set_phone(request: HttpRequest, phone: str):
    services.get_user_serv().set_phone_on_user(request.user.telegram_id, phone)
    return HttpResponse(status=HTTPStatus.OK)


@api.get("/favorites", auth=HTTPJWTAuth())
def get_favorites(request: HttpRequest) -> list[TelegramUserOut]:
    return services.get_favorites_serv().get_favorites(request.user.telegram_id)


@api.post("/favorites", auth=HTTPJWTAuth())
def add_favorites(request: HttpRequest, favorite: TelegramUserIn):
    services.get_favorites_serv().add_favorite(request.user.telegram_id, favorite.username)
    return HttpResponse(status=HTTPStatus.OK)


@api.delete("/favorites", auth=HTTPJWTAuth())
def del_favorites(request: HttpRequest, favorite: TelegramUserIn):
    services.get_favorites_serv().delete_favorite(request.user.telegram_id, favorite.username)
    return HttpResponse(status=HTTPStatus.OK)


@api.get("/cards", auth=HTTPJWTAuth())
def get_cards(request: HttpRequest) -> list[BankCardOut]:
    return services.get_bank_serv().get_user_cards(request.user.telegram_id)


@api.post("/transfer", auth=HTTPJWTAuth())
def send_money(request: HttpRequest, transfer: TransferIn):
    services.get_money_trans_serv().send_money_by_card(telegram_id=request.user.telegram_id,
                                                       money_source=transfer.money_source,
                                                       card_number=transfer.card_number,
                                                       money_amount=transfer.money_amount)
    return HttpResponse(status=HTTPStatus.OK)
