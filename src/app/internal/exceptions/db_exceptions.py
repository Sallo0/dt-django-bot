class UserDoesntExistException(Exception):
    pass


class UserNotInFavoriteException(Exception):
    pass


class SameUserException(Exception):
    pass


class SameAccountException(Exception):
    pass


class AccountDoesntExistException(Exception):
    pass


class CardDoesntExistException(Exception):
    pass


class NotEnoughMoneyException(Exception):
    pass
