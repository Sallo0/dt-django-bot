from django.contrib.auth.hashers import make_password
from django.db import transaction

from app.internal.entities import AuthSchema
from app.internal.models import TelegramUser, AuthToken
from config import settings


class AuthRepository:
    @transaction.atomic()
    def set_password_for_user(self, telegram_id: int, password: str) -> bool:
        user = TelegramUser.objects.select_for_update().filter(telegram_id=telegram_id)
        if not user.exists():
            return False
        user = user.first()
        user.password = make_password(password, salt=settings.PASSWORD_SALT)
        user.save(update_fields=["password"])
        return True

    def check_password(self, user: TelegramUser, password: str) -> bool:
        return user.password == make_password(password, salt=settings.PASSWORD_SALT)

    @transaction.atomic()
    def set_refresh_token(self, user: TelegramUser, refresh_token: str) -> None:
        AuthToken.objects.create(jti=refresh_token, user=user)

    def get_token(self, refresh_token: str) -> AuthToken | None:
        token = AuthToken.objects.filter(jti=refresh_token)
        if not token.exists():
            return None
        return token.first()

    def revoke_all_tokens(self, user: TelegramUser) -> None:
        user.refresh_tokens.update(revoked=True)
        user.save()

    def revoke_token(self, issued_token: AuthToken) -> None:
        issued_token.revoked = True
        issued_token.save(update_fields=["revoked"])
