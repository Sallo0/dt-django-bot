from app.internal.exceptions import UserDoesntExistException
from app.internal.models import BankAccount, BankCard, TelegramUser


class TelegramUserBankRepository:
    def get_user_accounts(self, telegram_id: int) -> BankAccount:
        bank_accounts = TelegramUser.objects.filter(telegram_id=telegram_id).first().accounts.all()
        return bank_accounts

    def get_user_cards(self, telegram_id: int) -> BankCard:
        bank_cards = (
            TelegramUser.objects.filter(telegram_id=telegram_id)
            .first()
            .cards.prefetch_related("account", "owner")
            .all()
        )
        return bank_cards

    def get_user_cards_by_username(self, username: str) -> list[BankCard]:
        user = TelegramUser.objects.filter(username=username).first()

        if user is None:
            raise UserDoesntExistException

        bank_cards = (
            TelegramUser.objects.filter(username=username).first().cards.prefetch_related("account", "owner").all()
        )
        return bank_cards

    def get_user_card_information(self, telegram_id: int, card_number: str) -> BankCard:
        card = (
            TelegramUser.objects.filter(telegram_id=telegram_id)
            .first()
            .cards.prefetch_related("account", "owner")
            .filter(card_number=card_number)
            .first()
        )
        return card

    def get_user_account_information(self, telegram_id: int, account_number: str) -> BankAccount:
        account = (
            TelegramUser.objects.filter(telegram_id=telegram_id)
            .first()
            .accounts.filter(account_number=account_number)
            .first()
        )
        return account

    def get_account(self, account_number: str) -> BankAccount:
        return BankAccount.objects.filter(account_number=account_number).first()

    def get_card(self, card_number: str) -> BankCard:
        return BankCard.objects.filter(card_number=card_number).first()
