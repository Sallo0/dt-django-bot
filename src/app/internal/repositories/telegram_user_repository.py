from app.internal.models import TelegramUser


class TelegramUserRepository:
    def create_user(self, telegram_id: int, username: str):
        TelegramUser.objects.get_or_create(telegram_id=telegram_id, username=username)

    def set_phone_on_user(self, telegram_id: int, phone: str):
        TelegramUser.objects.filter(telegram_id=telegram_id).update(phone_number=phone)

    def set_first_name_on_user(self, telegram_id: int, first_name: str):
        TelegramUser.objects.filter(telegram_id=telegram_id).update(first_name=first_name)

    def set_last_name_on_user(self, telegram_id: int, last_name: str):
        TelegramUser.objects.filter(telegram_id=telegram_id).update(last_name=last_name)

    def get_user(self, telegram_id: int):
        user = TelegramUser.objects.filter(telegram_id=telegram_id).first()
        return user

    def get_user_by_phone(self, phone: str):
        user = TelegramUser.objects.filter(phone_number=phone)
        if not user.exists():
            return None
        return user.first()
