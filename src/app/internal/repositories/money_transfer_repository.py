from decimal import Decimal

from django.db import transaction

from app.internal.exceptions import (
    AccountDoesntExistException,
    CardDoesntExistException,
    NotEnoughMoneyException,
    SameAccountException,
)
from app.internal.models import BankAccount, BankCard, TelegramUser, Transfer


class MoneyTransferRepository:
    def send_money_by_card(self, telegram_id: int, money_source: str, card_number: str, money_amount: float):
        user = TelegramUser.objects.filter(telegram_id=telegram_id).first()
        target_card = BankCard.objects.filter(card_number=card_number).first()

        if target_card is None:
            raise CardDoesntExistException

        user_account: BankAccount = user.cards.filter(card_number=money_source).first().account

        target_card_account: BankAccount = target_card.account

        if user_account == target_card_account:
            raise SameAccountException

        if user_account.balance < Decimal(money_amount):
            raise NotEnoughMoneyException

        with transaction.atomic():
            user_account.balance -= Decimal(money_amount)
            user_account.save()

            target_card_account.balance += Decimal(money_amount)
            target_card_account.save()

            Transfer(
                source_account=user_account,
                source_card=user.cards.filter(card_number=money_source).first(),
                transfer_initiator=user,
                target_account=target_card_account,
                target_user=target_card_account.owner,
                target_card=target_card,
                money_amount=Decimal(money_amount),
            ).save()

    def send_money_by_account(self, telegram_id: int, money_source: str, account_number: str, money_amount: float):
        user = TelegramUser.objects.filter(telegram_id=telegram_id).first()
        target_account = BankAccount.objects.filter(account_number=account_number).first()

        user_account: BankAccount = user.cards.filter(card_number=money_source).first().account

        if target_account is None:
            raise AccountDoesntExistException

        if user_account == target_account:
            raise SameAccountException

        if user_account.balance < Decimal(money_amount):
            raise NotEnoughMoneyException

        with transaction.atomic():
            user_account.balance -= Decimal(money_amount)
            user_account.save()

            target_account.balance += Decimal(money_amount)
            target_account.save()

    def get_user_transfers(self, telegram_id: int):
        return Transfer.objects.filter(transfer_initiator__telegram_id=telegram_id).all()

    def get_user_receipts(self, telegram_id: int):
        return Transfer.objects.filter(target_user__telegram_id=telegram_id).all()
