from app.internal.exceptions import SameUserException, UserDoesntExistException, UserNotInFavoriteException
from app.internal.models import TelegramUser


class FavoritesRepository:
    def get_favorites(self, telegram_id: int):
        favorites = TelegramUser.objects.filter(telegram_id=telegram_id).first().favorites.all()
        return favorites

    def add_favorite(self, telegram_id: int, favorite_username: str):
        user = TelegramUser.objects.filter(telegram_id=telegram_id).first()
        favorite = TelegramUser.objects.filter(username=favorite_username).first()

        if favorite is None:
            raise UserDoesntExistException

        user.favorites.add(favorite)

    def delete_favorite(self, telegram_id: int, favorite_username: str):
        user: TelegramUser = TelegramUser.objects.filter(telegram_id=telegram_id).first()
        favorite: TelegramUser = user.favorites.filter(username=favorite_username).first()

        if favorite is None:
            raise UserNotInFavoriteException

        user.favorites.remove(favorite)
