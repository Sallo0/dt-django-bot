from django.urls import path

from app.internal.transport.rest import api

urlpatterns = [path("", api.urls)]
