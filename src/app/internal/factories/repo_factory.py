from app.internal.repositories import (
    AuthRepository,
    FavoritesRepository,
    MoneyTransferRepository,
    TelegramUserBankRepository,
    TelegramUserRepository,
)


class RepoFactory:
    def __init__(self):
        self._auth_repo = AuthRepository()
        self._favorites_repo = FavoritesRepository()
        self._money_transfer_repo = MoneyTransferRepository()
        self._tg_user_repo = TelegramUserRepository()
        self._bank_repo = TelegramUserBankRepository()

    def get_auth_repo(self):
        return self._auth_repo

    def get_favorites_repo(self):
        return self._favorites_repo

    def get_money_transfer_repo(self):
        return self._money_transfer_repo

    def get_tg_user_repo(self):
        return self._tg_user_repo

    def get_bank_repo(self):
        return self._bank_repo
