from app.internal.factories import RepoFactory
from app.internal.services import (
    AuthService,
    FavoritesService,
    MoneyTransferService,
    TelegramUserBankService,
    TelegramUserService,
)


class ServicesFactory:
    def __init__(self, repo_factory: RepoFactory):
        self._auth_serv = AuthService(repo_factory)
        self._favorites_serv = FavoritesService(repo_factory)
        self._tg_user_serv = TelegramUserService(repo_factory)
        self._bank_serv = TelegramUserBankService(repo_factory)
        self._money_transfer_serv = MoneyTransferService(repo_factory)

    def get_auth_serv(self):
        return self._auth_serv

    def get_favorites_serv(self):
        return self._favorites_serv

    def get_user_serv(self):
        return self._tg_user_serv

    def get_bank_serv(self):
        return self._bank_serv

    def get_money_trans_serv(self):
        return self._money_transfer_serv
