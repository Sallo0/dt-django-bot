from app.internal.entities import BankAccountOut, BankAccountSchema, BankCardOut, TelegramUserSchema
from app.internal.exceptions import AccountDoesntExistException, CardDoesntExistException
from app.internal.factories import RepoFactory


class TelegramUserBankService:
    def __init__(self, repo_factory: RepoFactory):
        self.repos = repo_factory

    def get_user_accounts(self, telegram_id: int) -> list[BankAccountOut]:
        bank_accounts = self.repos.get_bank_repo().get_user_accounts(telegram_id=telegram_id)
        bank_accounts_schemas = [
            BankAccountOut(
                account_number=account.account_number,
                balance=account.balance,
                currency=account.currency,
                owner=account.owner.username,
            )
            for account in bank_accounts
        ]

        return bank_accounts_schemas

    def get_user_cards(self, telegram_id: int) -> list[BankCardOut]:
        bank_cards = self.repos.get_bank_repo().get_user_cards(telegram_id=telegram_id)
        bank_cards_schemas = [
            BankCardOut(
                balance=bank_card.account.balance,
                card_number=bank_card.card_number,
                account=bank_card.account.account_number,
                owner=bank_card.owner.username,
                currency=bank_card.account.currency,
            )
            for bank_card in bank_cards
        ]

        return bank_cards_schemas

    def get_user_cards_by_username(self, username: str) -> list[BankCardOut]:
        bank_cards = self.repos.get_bank_repo().get_user_cards_by_username(username=username)
        bank_cards_schemas = [
            BankCardOut(
                balance=bank_card.account.balance,
                card_number=bank_card.card_number,
                account=bank_card.account.account_number,
                owner=bank_card.owner.username,
                currency=bank_card.account.currency,
            )
            for bank_card in bank_cards
        ]

        return bank_cards_schemas

    def get_user_card_information(self, telegram_id: int, card_number: str) -> BankCardOut:
        card = self.repos.get_bank_repo().get_user_card_information(telegram_id=telegram_id, card_number=card_number)

        if card is None:
            raise CardDoesntExistException

        account = BankAccountSchema.from_orm(card.account)
        owner = TelegramUserSchema.from_orm(card.owner)

        return BankCardOut(
            card_number=card.card_number,
            account=account.account_number,
            owner=owner.username,
            balance=card.account.balance,
            currency=account.currency,
        )

    def get_card(self, card_number: str) -> BankCardOut:
        card = self.repos.get_bank_repo().get_card(card_number=card_number)

        if card is None:
            raise CardDoesntExistException

        account = BankAccountSchema.from_orm(card.account)
        owner = TelegramUserSchema.from_orm(card.owner)

        return BankCardOut(
            card_number=card.card_number,
            account=account.account_number,
            owner=owner.username,
            balance=card.account.balance,
            currency=account.currency,
        )

    def get_user_account_information(self, telegram_id: int, account_number: str) -> BankAccountOut:
        account = self.repos.get_bank_repo().get_user_account_information(
            telegram_id=telegram_id, account_number=account_number
        )

        if account is None:
            raise AccountDoesntExistException

        return BankAccountOut(
            account_number=account.account_number,
            balance=account.balance,
            currency=account.currency,
            owner=account.owner.username,
        )

    def get_account(self, account_number: str) -> BankAccountOut:
        account = self.repos.get_bank_repo().get_account(account_number=account_number)

        if account is None:
            raise AccountDoesntExistException

        return BankAccountOut(
            account_number=account.account_number,
            balance=account.balance,
            currency=account.currency,
            owner=account.owner.username,
        )
