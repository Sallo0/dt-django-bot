from app.internal.entities import TelegramUserOut
from app.internal.factories import RepoFactory


class FavoritesService:
    def __init__(self, repo_factory: RepoFactory):
        self.repos = repo_factory

    def get_favorites(self, telegram_id: int) -> list[TelegramUserOut]:
        favorites = self.repos.get_favorites_repo().get_favorites(telegram_id=telegram_id)
        return list(map(TelegramUserOut.from_orm, favorites))

    def add_favorite(self, telegram_id: int, favorite_username: str):
        self.repos.get_favorites_repo().add_favorite(telegram_id=telegram_id, favorite_username=favorite_username)

    def delete_favorite(self, telegram_id: int, favorite_username: str):
        self.repos.get_favorites_repo().delete_favorite(telegram_id=telegram_id, favorite_username=favorite_username)
