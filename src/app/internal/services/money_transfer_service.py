from app.internal.entities.transfer_entity import TransferSchema
from app.internal.factories import RepoFactory
from app.internal.models import Transfer


class MoneyTransferService:
    def __init__(self, repo_factory: RepoFactory):
        self.repos = repo_factory

    def send_money_by_card(self, telegram_id: int, money_source: str, card_number: str, money_amount: float):
        self.repos.get_money_transfer_repo().send_money_by_card(
            telegram_id=telegram_id, money_source=money_source, card_number=card_number, money_amount=money_amount
        )

    def send_money_by_account(self, telegram_id: int, money_source: str, account_number: str, money_amount: float):
        self.repos.get_money_transfer_repo().send_money_by_account(
            telegram_id=telegram_id, money_source=money_source, account_number=account_number, money_amount=money_amount
        )

    def get_user_transfers(self, telegram_id: int):
        transfers: list[Transfer] = self.repos.get_money_transfer_repo().get_user_transfers(telegram_id)
        return [
            TransferSchema(
                source_account=transfer.source_account.account_number,
                source_card=transfer.source_card.card_number,
                transfer_initiator=transfer.transfer_initiator.username,
                target_account=transfer.target_account.account_number,
                target_card=transfer.target_card.card_number,
                target_user=transfer.target_user.username,
                money_amount=transfer.money_amount,
            )
            for transfer in transfers
        ]

    def get_user_receipts(self, telegram_id: int):
        receipts = self.repos.get_money_transfer_repo().get_user_receipts(telegram_id)
        return [
            TransferSchema(
                source_account=transfer.source_account.account_number,
                source_card=transfer.source_card.card_number,
                transfer_initiator=transfer.transfer_initiator.username,
                target_account=transfer.target_account.account_number,
                target_card=transfer.target_card.card_number,
                target_user=transfer.target_user.username,
                money_amount=transfer.money_amount,
            )
            for transfer in receipts
        ]
