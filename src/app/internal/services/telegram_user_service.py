from app.internal.entities import TelegramUserOut
from app.internal.factories import RepoFactory


class TelegramUserService:
    def __init__(self, repo_factory: RepoFactory):
        self.repos = repo_factory

    def create_user(self, telegram_id: int, username: str):
        self.repos.get_tg_user_repo().create_user(telegram_id=telegram_id, username=username)

    def set_phone_on_user(self, telegram_id: int, phone: str):
        self.repos.get_tg_user_repo().set_phone_on_user(telegram_id=telegram_id, phone=phone)

    def set_first_name_on_user(self, telegram_id: int, first_name: str):
        self.repos.get_tg_user_repo().set_first_name_on_user(telegram_id=telegram_id, first_name=first_name)

    def set_last_name_on_user(self, telegram_id: int, last_name: str):
        self.repos.get_tg_user_repo().set_last_name_on_user(telegram_id=telegram_id, last_name=last_name)

    def get_user(self, telegram_id: int) -> TelegramUserOut:
        user = self.repos.get_tg_user_repo().get_user(telegram_id=telegram_id)
        if user is None:
            return None
        return TelegramUserOut.from_orm(user)

    def get_user_by_phone(self, phone: str):
        user = self.repos.get_tg_user_repo().get_user_by_phone(phone)
        if user is None:
            return None
        return user
