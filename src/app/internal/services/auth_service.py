import datetime
from datetime import timedelta

import jwt
import pytz
from django.db import transaction

from app.internal.entities import AuthSchema
from app.internal.factories import RepoFactory
from app.internal.models import AuthToken, TelegramUser
from config import settings


class AuthService:
    def __init__(self, repo_factory: RepoFactory):
        self.repos = repo_factory

    @staticmethod
    def _get_now() -> datetime.datetime:
        return pytz.UTC.localize(datetime.datetime.utcnow())

    @staticmethod
    def _utc_from_timestamp(timestamp) -> datetime.datetime:
        return datetime.datetime.fromtimestamp(timestamp, tz=pytz.UTC)

    def generate_access_and_refresh_tokens(self, user: TelegramUser) -> AuthSchema:
        access_token = self._generate_access_token(user.telegram_id)
        refresh_token = self._generate_refresh_token()
        self.repos.get_auth_repo().set_refresh_token(user, refresh_token)
        return AuthSchema(access_token=access_token, refresh_token=refresh_token)

    def get_token(self, refresh_token: str) -> AuthToken | None:
        return self.repos.get_auth_repo().get_token(refresh_token)

    def revoke_all_tokens(self, user: TelegramUser) -> None:
        return self.repos.get_auth_repo().revoke_all_tokens(user)

    def revoke_token(self, issued_token: AuthToken) -> None:
        return self.repos.get_auth_repo().revoke_token(issued_token)

    def has_token_expired(self, token: str) -> bool:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return True

        exp_time = payload.get("exp")

        if exp_time is None:
            return True

        return self._utc_from_timestamp(exp_time) < self._get_now()

    def get_telegram_id(self, token: str) -> int | None:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return None

        return payload.get("id")

    def _generate_access_token(self, telegram_id: int) -> str:
        dt = self._get_now() + timedelta(minutes=settings.JWT_ACCESS_TOKEN_EXP_TIME)

        return jwt.encode({"id": telegram_id, "exp": int(dt.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256")

    def _generate_refresh_token(self) -> str:
        dt = self._get_now() + timedelta(settings.JWT_REFRESH_TOKEN_EXP_TIME)

        return jwt.encode({"exp": int(dt.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256")

    def set_password_for_user(self, telegram_id: int, password: str) -> bool:
        return self.repos.get_auth_repo().set_password_for_user(telegram_id, password)

    def check_password(self, user: TelegramUser, password: str):
        return self.repos.get_auth_repo().check_password(user, password)
