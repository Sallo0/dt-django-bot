from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path


def admin_redirect(request):
    return redirect("admin/")


urlpatterns = [
    path("", admin_redirect),
    path("admin/", admin.site.urls),
    path("api/", include("app.internal.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
