import json

import pytest
from django.http import HttpResponse


@pytest.mark.django_db
def test_me(client, test_user):
    response: HttpResponse = client.get("/api/me/123")
    assert response.status_code == 404
