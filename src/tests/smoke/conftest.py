import pytest

from app.internal.models import TelegramUser


@pytest.fixture(scope="function")
def test_user(telegram_id="2", username="username2", first_name=None, last_name=None, phone_number=None):
    user = TelegramUser(
        telegram_id=telegram_id,
        username=username,
        first_name=first_name,
        last_name=last_name,
        phone_number=phone_number,
    )

    user.save()
    return user
