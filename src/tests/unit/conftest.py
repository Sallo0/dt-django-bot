import pytest

from app.internal.models import BankAccount, BankCard, TelegramUser


@pytest.fixture(scope="function")
def test_user(
    telegram_id="1", username="username", first_name="FirstName", last_name="LastName", phone_number="+79530407656"
):
    return TelegramUser(
        telegram_id=telegram_id,
        username=username,
        first_name=first_name,
        last_name=last_name,
        phone_number=phone_number,
    )


@pytest.fixture(scope="function")
def test_bank_account(test_user: TelegramUser, account_number="7" * 20):
    return BankAccount(account_number=account_number, balance=500000, currency="RUB", owner=test_user)


@pytest.fixture(scope="function")
def test_bank_card(test_user: TelegramUser, test_bank_account: BankAccount, card_number="7" * 16):
    return BankCard(owner=test_user, card_number=card_number, account=test_bank_account)
