import pytest

from app.internal.entities import BankAccountOut, BankCardOut
from app.internal.exceptions import AccountDoesntExistException, CardDoesntExistException
from app.internal.repositories import MoneyTransferRepository, TelegramUserBankRepository
from app.internal.services import TelegramUserBankService


class TestTelegramUserBankService:
    def test_get_user_card_information_card_not_exist(self, mocker):
        mock_repository = mocker.patch.object(TelegramUserBankRepository, "get_user_card_information")
        mock_repository.return_value = None

        with pytest.raises(CardDoesntExistException):
            TelegramUserBankService.get_user_card_information(1, "1" * 16)

    def test_get_card_not_exist(self, mocker):
        mock_repository = mocker.patch.object(TelegramUserBankRepository, "get_card")
        mock_repository.return_value = None

        with pytest.raises(CardDoesntExistException):
            TelegramUserBankService.get_card("1" * 16)

    def test_get_user_account_information_account_not_exist(self, mocker):
        mock_repository = mocker.patch.object(TelegramUserBankRepository, "get_user_account_information")
        mock_repository.return_value = None

        with pytest.raises(AccountDoesntExistException):
            TelegramUserBankService.get_user_account_information(1, "1" * 20)

    def test_get_account_not_exist(self, mocker):
        mock_repository = mocker.patch.object(TelegramUserBankRepository, "get_account")
        mock_repository.return_value = None

        with pytest.raises(AccountDoesntExistException):
            TelegramUserBankService.get_account("1" * 20)
