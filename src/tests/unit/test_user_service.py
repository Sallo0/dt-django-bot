from app.internal.entities import TelegramUserOut
from app.internal.models import TelegramUser
from app.internal.repositories import FavoritesRepository, TelegramUserRepository
from app.internal.services import TelegramUserService


class TestTelegramUserService:
    def test_get_user(self, mocker, test_user: TelegramUser):
        mock_repository = mocker.patch.object(TelegramUserRepository, "get_user")
        mock_repository.return_value = test_user

        user = TelegramUserService.get_user(test_user.telegram_id)

        assert user.telegram_id == test_user.telegram_id
        assert user.username == test_user.username
        assert user.first_name == test_user.first_name
        assert user.last_name == test_user.last_name
        assert user.phone_number == test_user.phone_number

    def test_get_user_not_exist(self, mocker, test_user: TelegramUser):
        mock_repository = mocker.patch.object(TelegramUserRepository, "get_user")
        mock_repository.return_value = None

        user = TelegramUserService.get_user(test_user.telegram_id)

        assert user is None

    def test_get_favorites_empty_list(self, mocker, test_user: TelegramUser):
        mock_repository = mocker.patch.object(FavoritesRepository, "get_favorites")
        mock_repository.return_value = []

        favorites = TelegramUserService.get_favorites(test_user.telegram_id)

        assert favorites == []

    def test_get_favorites(self, mocker):
        test_user1 = TelegramUser(telegram_id=1, username="user1", phone_number="12345678901")
        test_user2 = TelegramUser(telegram_id=2, username="user2", phone_number="12345678902")
        test_user3 = TelegramUser(telegram_id=3, username="user3", phone_number="12345678903")

        mock_repository = mocker.patch.object(FavoritesRepository, "get_favorites")
        test_fav_list = [test_user1, test_user2, test_user3]
        mock_repository.return_value = test_fav_list

        favorites = TelegramUserService.get_favorites(123)

        assert favorites == list(map(TelegramUserOut.from_orm, test_fav_list))
