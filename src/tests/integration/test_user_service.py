import pytest

from app.internal.entities import TelegramUserOut
from app.internal.services import TelegramUserService


class TestTelegramUserService:
    @pytest.mark.django_db
    def test_get_user(self, test_user):
        user = TelegramUserService.get_user(telegram_id=test_user.telegram_id)
        assert user == TelegramUserOut.from_orm(test_user)

    @pytest.mark.django_db
    def test_set_phone_on_user(self, test_user):
        phone = "89999909999"
        TelegramUserService.set_phone_on_user(telegram_id=test_user.telegram_id, phone=phone)
        user = TelegramUserService.get_user(telegram_id=test_user.telegram_id)
        assert phone == user.phone_number

    @pytest.mark.django_db
    def test_set_fio(self, test_user):
        first_name = "FirstName"
        last_name = "LastName"
        TelegramUserService.set_first_name_on_user(telegram_id=test_user.telegram_id, first_name=first_name)
        TelegramUserService.set_last_name_on_user(telegram_id=test_user.telegram_id, last_name=last_name)
        user = TelegramUserService.get_user(telegram_id=test_user.telegram_id)
        assert first_name == user.first_name
        assert last_name == user.last_name

    @pytest.mark.django_db
    def test_add_favorite(self, test_user):
        TelegramUserService.add_favorite(telegram_id=test_user.telegram_id, favorite_username=test_user.username)
        favorites = TelegramUserService.get_favorites(telegram_id=test_user.telegram_id)
        assert len(favorites) == 1

    @pytest.mark.django_db
    def test_delete_favorite(self, test_user):
        TelegramUserService.add_favorite(telegram_id=test_user.telegram_id, favorite_username=test_user.username)
        TelegramUserService.delete_favorite(telegram_id=test_user.telegram_id, favorite_username=test_user.username)

        favorites = TelegramUserService.get_favorites(telegram_id=test_user.telegram_id)
        assert len(favorites) == 0
