import pytest

from app.internal.models import BankAccount, BankCard, TelegramUser


@pytest.fixture(scope="function")
def test_user(telegram_id="1", username="username", first_name=None, last_name=None, phone_number=None):
    user = TelegramUser(
        telegram_id=telegram_id,
        username=username,
        first_name=first_name,
        last_name=last_name,
        phone_number=phone_number,
    )

    user.save()
    return user


@pytest.fixture(scope="function")
def test_user2(telegram_id="2", username="username2", first_name=None, last_name=None, phone_number=None):
    user = TelegramUser(
        telegram_id=telegram_id,
        username=username,
        first_name=first_name,
        last_name=last_name,
        phone_number=phone_number,
    )

    user.save()
    return user


@pytest.fixture(scope="function")
def test_bank_account(test_user: TelegramUser, account_number="7" * 20):
    account = BankAccount(account_number=account_number, balance=500000, currency="RUB", owner=test_user)
    account.save()
    return account


@pytest.fixture(scope="function")
def test_bank_account2(test_user2: TelegramUser, account_number="8" * 20):
    account = BankAccount(account_number=account_number, balance=500000, currency="RUB", owner=test_user)
    account.save()
    return account


@pytest.fixture(scope="function")
def test_bank_card(test_user: TelegramUser, test_bank_account: BankAccount, card_number="7" * 16):
    card = BankCard(owner=test_user, card_number=card_number, account=test_bank_account)
    card.save()
    return card


@pytest.fixture(scope="function")
def test_bank_card2(test_user2: TelegramUser, test_bank_account2: BankAccount, card_number="8" * 16):
    card = BankCard(owner=test_user, card_number=card_number, account=test_bank_account)
    card.save()
    return card
