import pytest

from app.internal.exceptions import AccountDoesntExistException, CardDoesntExistException, SameAccountException
from app.internal.models import BankAccount
from app.internal.services import TelegramUserBankService


class TestTelegramUserBankService:
    @pytest.mark.django_db
    def test_send_money_by_card_same_account(self, test_user, test_bank_card, test_bank_account):
        with pytest.raises(SameAccountException):
            TelegramUserBankService.send_money_by_card(
                telegram_id=test_user.telegram_id,
                money_source=test_bank_card.card_number,
                card_number=test_bank_card.card_number,
                money_amount=1001,
            )

    @pytest.mark.django_db
    def test_send_money_by_account_same_account(self, test_user, test_bank_card, test_bank_account):
        with pytest.raises(SameAccountException):
            TelegramUserBankService.send_money_by_account(
                telegram_id=test_user.telegram_id,
                money_source=test_bank_card.card_number,
                account_number=test_bank_account.account_number,
                money_amount=1001,
            )

    @pytest.mark.django_db
    def test_send_money_by_card_dest_not_exist(self, test_user, test_bank_card, test_bank_account):
        with pytest.raises(CardDoesntExistException):
            TelegramUserBankService.send_money_by_card(
                telegram_id=test_user.telegram_id,
                money_source=test_bank_card.card_number,
                card_number="1234",
                money_amount=1001,
            )

    @pytest.mark.django_db
    def test_send_money_by_account_dest_not_exist(self, test_user, test_bank_card, test_bank_account):
        with pytest.raises(AccountDoesntExistException):
            TelegramUserBankService.send_money_by_account(
                telegram_id=test_user.telegram_id,
                money_source=test_bank_card.card_number,
                account_number="1234",
                money_amount=1001,
            )
